from flask import Flask
from flask import render_template, g, session
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager, login_user, logout_user, current_user, login_required
import logging
from logging.handlers import RotatingFileHandler
import sys
from os.path import dirname, exists, join
sys.path.append(join(dirname(__file__), '..'))
from werkzeug.datastructures import CallbackDict
from flask.sessions import SessionInterface, SessionMixin
from itsdangerous import URLSafeTimedSerializer, BadSignature


app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {'DB': 'data_dash'}
app.config["SECRET_KEY"] = "3D23C95BCADBD768D712E9787EB66"
##app.config["CSRF_ENABLED"] = True
# lm = LoginManager()
# lm.init_app(app)
# lm.login_view = 'login'

db = MongoEngine(app)


if __name__ == '__main__':
	handler = RotatingFileHandler('traffik_errors.log', maxBytes=10000, backupCount=1)
	handler.setLevel(logging.WARNING)
	app.logger.addHandler(handler)
	app.run()

# @lm.user_loader
# def load_user(id):
#     return User.query.get(int(id))

# @app.before_request
# def before_request():
# 	g.user = current_user 

from data_dashboard.views import users, requests
from data_dashboard.models import row_data, users


class ItsdangerousSession(CallbackDict, SessionMixin):

    def __init__(self, initial=None):
        def on_update(self):
            self.modified = True
        CallbackDict.__init__(self, initial, on_update)
        self.modified = False


class ItsdangerousSessionInterface(SessionInterface):
    salt = 'cookie-session'
    session_class = ItsdangerousSession

    def get_serializer(self, app):
        if not app.secret_key:
            return None
        return URLSafeTimedSerializer(app.secret_key, 
                                      salt=self.salt)

    def open_session(self, app, request):
        s = self.get_serializer(app)
        if s is None:
            return None
        val = request.cookies.get(app.session_cookie_name)
        if not val:
            return self.session_class()
        max_age = app.permanent_session_lifetime.total_seconds()
        try:
            data = s.loads(val, max_age=max_age)
            return self.session_class(data)
        except BadSignature:
            return self.session_class()

    def save_session(self, app, session, response):
        domain = self.get_cookie_domain(app)
        if not session:
            if session.modified:
                response.delete_cookie(app.session_cookie_name,
                                   domain=domain)
            return
        expires = self.get_expiration_time(app, session)
        val = self.get_serializer(app).dumps(dict(session))
        response.set_cookie(app.session_cookie_name, val,
                            expires=expires, httponly=False,
                            domain=domain)


app.session_interface = ItsdangerousSessionInterface()
