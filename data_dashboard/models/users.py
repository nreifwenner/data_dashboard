import datetime
from flask import url_for
from mongoengine import *
from data_dashboard import db

class User(db.Document):
	email = StringField(required=True)
	first_name = StringField(required=True)
	last_name = StringField(required=True)
	safe_id = StringField(required=True)
	


