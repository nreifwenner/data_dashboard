import datetime
from flask import url_for
from mongoengine import *
from data_dashboard import db
from data_dashboard.models.users import User
from data_dashboard.models.notes import Note

class User_data(db.Document):
	user_id = StringField(required = True)
	user = ReferenceField(User, dbref=False)
	pref_view = StringField()
	pref_process_filter = StringField()
	pref_product_filter = StringField()
	pref_act_filter = StringField()
	pref_manager_filter = StringField()
