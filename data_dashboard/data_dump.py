from data_dashboard.data_extractor import DataExtractor
from data_dashboard.models.row_data import RowData, Thresholds
import os
import platform
from datetime import datetime
import pytz

def ardwaag_location():
	if platform.system() == 'Linux':
		c = '/media/pub3008/General/Customer_Ops_Analytic_Reports/Dashboard_reports/ardwaag_ready_activities_rpt.csv'
	elif platform.system() == 'Windows':
		c = r'\\fl-custops-nas\pub3008\General\Customer_Ops_Analytic_Reports\Dashboard_reports\ardwaag_ready_activities_rpt.csv'

def modification_date(filename):
    t = os.path.getmtime(filename)
    return datetime.utcfromtimestamp(t)

#loop through spreadsheet lists, assigning values to DB keys\
def output_data():
	# convert windows file path to unix
	if platform.system() == 'Linux':
		c = '/media/pub3008/General/Development/DashboardReports/AllRealtimeDynamicWorkloadAtAGlance.xls'
	elif platform.system() == 'Windows':
		c = r'\\fl-custops-nas\pub3008\General\Development\DashboardReports\AllRealtimeDynamicWorkloadAtAGlance.xls'
	# c =  '/media/pub3008/General/Development/DashboardReports/AllRealtimeDynamicWorkloadAtAGlance.xls'
	get_data = DataExtractor(c)
	timestamp = datetime.now(pytz.utc)
	modified = modification_date(c);
	for lists in get_data.workbook_extract()[1:]:
		new_row = RowData()
		new_row.timestamp = timestamp
		new_row.project_age = int(lists[0])
		if lists[1] == "":
			new_row.participant_last_name = "Unassigned"
		else:
			new_row.participant_last_name = lists[1]
		new_row.participant_first_name = lists[2]
		new_row.org_unit = lists[3] 
		new_row.pm_name = lists[4] 
		new_row.firm_name = lists[5] 
		new_row.wld_id = int(lists[6]) 
		new_row.sub_id = int(lists[7])
		if lists[8] == "":
			new_row.logical_site = 0
		else:
			new_row.logical_site = int(lists[8]) 
		new_row.sap_type = lists[9]
		if lists[10] == "":
			new_row.account_value = 0.00
		else:
			new_row.account_value = float(lists[10]) 
		new_row.product = lists[11] 
		new_row.process_description = lists[12] 
		new_row.activity_description = lists[13] 
		new_row.activity_age = int(lists[14]) 
		new_row.activity_due = lists[15]
		new_row.completed_date = lists[16]
		new_row.status = lists[17] 
		new_row.bpm_id = int(lists[18]) 
		new_row.cdc = lists[19] 
		new_row.rsm = lists[20] 
		new_row.writer = lists[21] 
		new_row.designer = lists[22] 
		new_row.seo = lists[23] 
		new_row.line_item_status = lists[24] 
		new_row.activity_status = lists[25] 
		new_row.project_status = lists[26] 
		new_row.priority = lists[27]
		if lists[28] == "":
			new_row.staff_manager = "Unassigned"
		else:
			new_row.staff_manager = lists[28]
		new_row.date_modified = modified
		#print (lists)
		# new_sheet.row_list.append(new_row)
		new_row.save()
	print (get_data.date_mod)
	return

def saveThreshold(data):
	new_threshold = Thresholds()
	new_threshold.activity = data['activity']
	new_threshold.green_project = data['green_project']
	new_threshold.yellow_project = data['yellow_project']
	new_threshold.save()
	return
