'use strict';

describe('IndexController', function() {
    //initialise module
    beforeEach(angular.mock.module('Traffik'));

    //params initialised in scope for tests
    var ctrl, scope, $httpBackend;

    beforeEach(angular.mock.inject(function(_$httpBackend_, $rootScope, $controller) {
        //get controller from $controller provider
       
        $httpBackend = _$httpBackend_;
        $httpBackend.when('GET', '/thresholds').respond([{"activity": "Design Prototype Due"}, {"activity": "Complete F.E.D."}]);
        this.scope = $rootScope.$new();
        ctrl = $controller('IndexController', {
            $scope: this.scope
        }); 
    }));

    it ('should have a current org unit defined', function() {

        expect(this.scope.currentOrg).toBeDefined(); 
    }); 
   it('should create thresholds object', function() {
     
     $httpBackend.flush();
    
     expect(this.scope.thresholds).toEqual([{activity: 'Complete F.E.D.'},
                                  {activity: 'Design Prototype Due'}]);
   });
   it('should have one or more filters options for each org ', function(){
    expect(this.scope.activitiesList).toBeGreaterThan(0);
   });
});