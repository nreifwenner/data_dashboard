app.factory('detailService', function($http, $rootScope, $q) {

    var pThresholdsObj;


    return {

        getDetail: function(currentSub) {
            var parsedLists = $q.defer();
            var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?Sub_Id=' + currentSub);
            var notesPromise = $http.get('/get_notes');
            notesPromise.then(function(notesResponse) {
                var rawNotes = notesResponse.data;
                var returnedNotes = [];
                for (var i = 0; i < rawNotes.length; i++) {

                    if ((currentSub * 1) === rawNotes[i].sub_id) {
                        var tempNotesObj = {};
                        tempNotesObj.activity = rawNotes[i].activity;
                        tempNotesObj.author = rawNotes[i].author;
                        tempNotesObj.content = rawNotes[i].content;
                        tempNotesObj.date = rawNotes[i].date;
                        tempNotesObj.safeID = rawNotes[i].safeID;
                        tempNotesObj.subID = rawNotes[i].sub_id;
                        tempNotesObj.rowData = JSON.parse(rawNotes[i].row_data);
                        tempNotesObj._id = rawNotes[i]._id;
                        returnedNotes.push(tempNotesObj);
                    }

                }
                rowPromise.then(function(response) {
                    var returnedData = response.data.results;
                    //for(var i= 0; i<returnedData; )
                    var newRows = [];
                    _.each(returnedData, function(project) {
                        var tempRow = {};
                        tempRow.project_age = project["#Project_Age_In_Days"];
                        if (project.activities[0].Last_Name === "") {
                            tempRow.participant_last_name = "Unassigned";
                        } else {
                            tempRow.participant_last_name = project.activities[0].Last_Name;
                        }
                        tempRow.participant_first_name = project.activities[0].First_Name;
                        tempRow.org_unit = project.activities[0].Org_Unit;
                        tempRow.pm_name = project.Project_Manager;
                        tempRow.firm_name = project.Firm_Name;
                        tempRow.wld_id = project.Wld_Id * 1;
                        tempRow.sub_id = project.Sub_Id * 1;
                        tempRow.logical_site = project.Logical_Site_Id * 1;
                        tempRow.sap_type = project.Sap_Account_Type;
                        if (project.Account_Value === "") {
                            tempRow.Account_Value = "0.00";
                        } else {
                            tempRow.account_value = parseFloat(project.Account_Value);
                        }

                        tempRow.product = project.Product;
                        tempRow.process_description = project.Process_Description;
                        tempRow.activity_description = project.activities[0].Activity_Description;
                        tempRow.activity_age = project.activities[0].Activity_Age_In_Business_Days;
                        tempRow.activity_due = project.activities[0].Activity_Due_Date;
                        tempRow.completed_date = project.Completed_Date;
                        tempRow.status = project.Status;
                        tempRow.bpm_id = project.Bpm_Project_Id * 1;
                        tempRow.cdc = project.Cdc;
                        tempRow.rsm = project.Rsm;
                        tempRow.writer = project.Writer;
                        tempRow.designer = project.Designer;
                        tempRow.seo = project.Seo;
                        tempRow.line_item_status = project.Line_Item_Status;
                        tempRow.activity_status = project.activities[0].Activity_Status;
                        tempRow.project_status = project.Project_Status;
                        tempRow.priority = project.Priority;
                        if (project.activities[0].Staff_Manager === "") {
                            tempRow.staff_manager = "Unassigned";
                        } else {
                            tempRow.staff_manager = project.activities[0].Staff_Manager;

                        }

                        newRows.push(tempRow);

                    });
                    $http.get('/thresholds').then(function(response) {

                        pThresholdsObj = response.data;
                        var rows = parseRowsAssignColors(newRows, pThresholdsObj, []);
                        parsedLists.resolve({
                            "rows": rows, //this is a really bad solution
                            "notes": returnedNotes

                        });
                    });
                });
            });
            return parsedLists.promise;
        }
    };
});


app.service('participantService', function($http, $q) {
    var parsedLists;
    var pThresholdsObj;
    return {

        getParticipant: function(participantFirst, participantLast) {
            var parsedLists = $q.defer();
            var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?First_Name' + participantFirst + '&Last_Name=' + participantLast).then(function(response) {
                var returnedData = response.data.results;

                var newRows = transformServiceRows(returnedData, null, participantFirst, participantLast);

                $http.get('/thresholds').then(function(response) {

                    pThresholdsObj = response.data;

                    parsedLists.resolve(parseRowsAssignColors(newRows, pThresholdsObj, []));

                });
            });

            return parsedLists.promise;
        }
    };
});

app.factory('thresholdServices', ['$http', '$q',
    function($http, $q) {

        return {
            getThresholds: function() {
                console.log("starting thresholds");
                var threshPromise = $http.get('/thresholds');
                return threshPromise;
            },
            setThreshold: function(thresholdData) {
                var returnData = $q.defer();
                var request = $http({
                    url: '/set_threshold',
                    method: "POST",
                    data: JSON.stringify(thresholdData),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                request.then(function(response) {
                    returnData.resolve(response.data);

                });
                return returnData.promise;
            },
            getActivitiesList: function() {
                $http.get('/activity_list').success(function(data) {
                    $rootScope.activityList = data;
                });
            }
        };
    }
]);


app.service('userService', function($http, $q) {
    return {
        resetUserPrefs: function(user) {

            var tempObj = {};
            tempObj.user_id = user.safe_id;
            var returnData = $q.defer();
            var request = $http({
                url: '/reset_user_prefs',
                method: "POST",
                data: JSON.stringify(tempObj),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            request.then(function(response) {
                returnData.resolve(response.data);

            });
            return returnData.promise;

        },
        getUser: function() {
            user = $q.defer();
            $http.get('/get_user').then(function(response) {
                user.resolve(response.data);
            });
            return user.promise;
        },
        getUserData: function() {
            userData = $q.defer();
            $http.get('/get_user_data').then(function(response) {
                userData.resolve(response.data);
            });
            return userData.promise;
        },
        setUserData: function(formData) {

            newUserData = {};
            returnData = $q.defer();
            if (formData.org !== "" || formData.org !== null) {
                newUserData.preferred_view = formData.org;
            } else {
                newUserData.preferredView = null;
            }
            if (formData.activity !== "" || formData.activity !== null) {
                newUserData.activity_filter = formData.activity;
            } else {
                newUserData.activity_filter = null;
            }
            if (formData.manager !== "" || formData.manger !== null) {
                newUserData.manager_filter = formData.manager;
            } else {
                newUserData.manager_filter = null;
            }
            if (formData.process !== "" || formData.process !== null) {
                newUserData.process_filter = formData.process;
            } else {
                newUserData.process_filter = null;
            }
            if (formData.product !== "" || formData.product !== null) {
                newUserData.product_filter = formData.product;
            } else {
                newUserData.product_filter = null;
            }

            var request = $http({
                url: '/update_user_data',
                method: "POST",
                data: JSON.stringify({
                    pref_view: formData.org,
                    pref_manager_filter: formData.manager,
                    pref_activity_filter: formData.activity,
                    pref_process_filter: formData.process,
                    pref_product_filter: formData.product
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            request.then(function(response) {
                returnData.resolve(response.data);

            });
            return returnData.promise;

        }


    };
});
app.factory('tempListService', ['$http',
    function($http) {
        return {
            getList: function(listType, orgUnit) {

                var processedOrgUnit = orgUnit.replace(/ /g, "");
                var nameConcat = listType + processedOrgUnit;
                var queryData = $http.get('/static/temp_json/' + nameConcat + '.json');
                return queryData;
            }
        };
    }
]);

app.service('tempLiveSearchService', function($http, $q) {

    var pThresholdsObj;
    return {

        getData: function(query) {
            //query is the org
            switch (query) {
                case "design":
                    convertedOrgUnit = "Org_Unit=Design";
                    break;

                case "development":
                    convertedOrgUnit = "Org_Unit=FED+BGL&Org_Unit=FED+EGN";
                    break;



                default:
                    convertedOrgUnit = "Org_Unit=Design";
                    break;

            }
            var notesPromise = $http.get('/get_notes');
            var parsedLists = $q.defer();


            var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?' + convertedOrgUnit + "&limit=200");

            notesPromise.then(function(notesResponse) {
                var rawNotes = notesResponse.data;
                var returnedNotes = [];
                for (var i = 0; i < rawNotes.length; i++) {
                    var tempNotesObj = {};
                    tempNotesObj.activity = rawNotes[i].activity;
                    tempNotesObj.author = rawNotes[i].author;
                    tempNotesObj.content = rawNotes[i].content;
                    tempNotesObj.date = rawNotes[i].date;
                    tempNotesObj.safeID = rawNotes[i].safeID;
                    tempNotesObj.subID = rawNotes[i].sub_id;
                    tempNotesObj.rowData = JSON.parse(rawNotes[i].row_data);
                    tempNotesObj._id = rawNotes[i]._id;
                    returnedNotes.push(tempNotesObj);

                }
                rowPromise.then(function(response) {
                    var returnedData = response.data.results;



                    newRows = transformServiceRows(returnedData, query);

                    $http.get('/thresholds').then(function(response) {



                        parsedLists.resolve(parseRowsAssignColors(newRows, response.data, returnedNotes));

                    });


                });
            });



            return parsedLists.promise;
        },
        getDateModified: function() {
            var url = '/get_date_modified';
            return $http.get(url);
        },
        scaffoldThresholds: function() {
            var returnData = $q.defer();
            var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?Org_Unit=FED+BGL&Org_Unit=FED+EGN&Org_Unit=Design&limit=999');
            rowPromise.then(function(response) {
                var returnedData = response.data.results;
                var threshArr = makeThresholdScaffolding(returnedData);
                var threshRequest = $http({
                    url: '/process_thresholds',
                    method: "POST",
                    data: JSON.stringify(threshArr),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                threshRequest.then(function(response) {

                    returnData.resolve(response.data);
                });
            });
            return returnData.promise;
        }
    };
});
app.service("notesService", function($http, $q) {
    return {
        writeNote: function(noteObj) {
            var returnData = $q.defer();
            if (!noteObj) {
                returnData.resolve("error");
                return returnData.promise;
            }
            var request = $http({
                url: '/write_note',
                method: "POST",
                data: JSON.stringify(noteObj),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            request.then(function(response) {
                returnData.resolve(response.data);

            });
            return returnData.promise;

        },
        readNotes: function() {
            var returnData = $q.defer();
            $http.get('/get_notes').then(function(response) {
                var rawNotes = response.data;
                var returnedNotes = [];
                for (var i = 0; i < rawNotes.length; i++) {
                    var tempNotesObj = {};
                    tempNotesObj.activity = rawNotes[i].activity;
                    tempNotesObj.author = rawNotes[i].author;
                    tempNotesObj.content = rawNotes[i].content;
                    tempNotesObj.date = rawNotes[i].date;
                    tempNotesObj.safeID = rawNotes[i].safeID;
                    tempNotesObj.subID = rawNotes[i].sub_id;
                    tempNotesObj.rowData = JSON.parse(rawNotes[i].row_data);
                    tempNotesObj._id = rawNotes[i]._id;
                    returnedNotes.push(tempNotesObj);
                }

                returnData.resolve(returnedNotes);
            });
            return returnData.promise;
        },
        updateNote: function(note, user) {


            if (user.safe_id !== note.safeID) {
                alert("you are not the author!");
                return "error";
            }
            var tempNotesObj = {};
            tempNotesObj.id = note._id;
            tempNotesObj.date = note.date;
            tempNotesObj.subID = note.subID;
            tempNotesObj.safeID = note.safeID;
            tempNotesObj.content = note.content;
            var request = $http({
                url: '/update_note',
                method: "POST",
                data: JSON.stringify(tempNotesObj),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return request;

        },

        deleteNote: function(id, user) {
            var tempObj = {};
            tempObj.safeID = user.safe_id;
            tempObj.note_id = id;
            var request = $http({
                url: '/delete_note',
                method: 'POST',
                data: JSON.stringify(tempObj),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return request;
        }
    };

});