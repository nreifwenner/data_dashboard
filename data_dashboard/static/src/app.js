var app = angular.module('Traffik', [
    'ui.router', 'ui.bootstrap.affix', 'ui.bootstrap', 'ui.bootstrap.tpls', 'ui.bootstrap.alert', 'googlechart', 'traffikDirectives'
]);
/*



             ##           ##
             ##           ##
      ###  #####   ###  #####   ###    ###
     ## ##  ##    #  ##  ##    ## ##  ## ##
      ##    ##     ####  ##    #####   ##
       ##  ##    ## ##  ##    ##        ##
    ## ##  ##    ## ##  ##    ## ##  ## ##
     ###    ##    ## ##  ##    ###    ###


*/
app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('design', {
            url: '/design',
            templateUrl: 'static/partials/landing.html',
            controller: 'mainCtrl'

        })

    .state('development', {
        url: '/development',
        templateUrl: 'static/partials/landing.html',
        controller: 'mainCtrl'

    })

    .state('detail', {
        url: '/detail/:id',
        templateUrl: 'static/partials/detail.content.html',
        controller: 'detailCtrl'
    })
        .state('participant', {
            url: '/participant/:id',
            templateUrl: 'static/partials/participant.content.html',
            controller: 'participantCtrl'
        })
        .state('thresholds', {
            url: '/set-thresholds',
            templateUrl: 'static/partials/threshold.content.html',
            controller: 'thresholdsCtrl'
        });
});
app.run(['$rootScope', '$state', '$stateParams', 'userService',
    function($rootScope, $state, $stateParams, userService) {
        $rootScope.pref = {};
        userService.getUserData().then(function(response) {
            var pref_view = '';
            var lastViewed = sessionStorage.getItem('lastViewed');

            var lastViewedParamsString = sessionStorage.getItem('lastViewedParams');
            if (lastViewedParamsString === '{"id":""}') {
                lastViewedParams = '';

            } else {

                lastViewedParams = JSON.parse(lastViewedParamsString);
            }


            $rootScope.userData = response;
            if (response.pref_view) {

                pref_view = response.pref_view.toLowerCase();
            }

            if (lastViewed) {


                $state.transitionTo(lastViewed, lastViewedParams);
            } else if (pref_view !== '') {
                $state.transitionTo(pref_view);
            } else {
                $state.transitionTo('design');
            }
        });
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;


    }
]);

/*



         ##          ##           ##   ##      ###    ##          ##
         ##          ##                ##     ## ##   ##          ##
      ####    ###  #####   ###   ##   ##     ##  ## #####  # ##  ##
     ## ##   ## ##  ##    #  ##  ##   ##     ##      ##    ###   ##
     #  ##   #####  ##     ####  ##   ##    ##       ##    ##    ##
    ##  #   ##     ##    ## ##  ##   ##     ##   #  ##    ##    ##
    ## ##   ## ##  ##    ## ##  ##   ##     ##  ##  ##    ##    ##
     ####    ###    ##    ## ## ##   ##      ####    ##   ##    ##


*/



app.controller('detailCtrl', ['detailService', '$state', '$stateParams', '$scope', '$modal', '$log', '$filter', '$rootScope', 'notesService', '$timeout',

    function(detailService, $state, $stateParams, $scope, $modal, $filter, $log, $rootScope, notesService, $timeout) {
        $scope.alerts = [];
        $scope.closeAlert = function(timeout) {

            $timeout(function() {
                $scope.alerts = [];
            }, timeout);
        };

        $scope.sub = $stateParams.id;
        $scope.detailError = false;
        $scope.detailRows = {};
        detailService.getDetail($scope.sub, $rootScope.notes).then(function(response) {

            $scope.detailRows = response.rows.rows;
            $scope.notesData.notes = response.notes;

            return $scope.detailRows;
        });

        $scope.notesData = {};
        $scope.notesData.notes = [];
        $scope.notesData.newNotes = [];


        $scope.open = function(idx) {

            var modalInstance = $modal.open({
                templateUrl: 'static/partials/addDetail.html',
                controller: addFieldCtrl,
                resolve: {
                    rows: function() {
                        if (!$scope.selected) {
                            $scope.selected = [];
                        }

                        $scope.detailRows.selected = $scope.selected;
                        return $scope.detailRows[idx];
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };


        $scope.nameSplit = function(name) {
            var nameRaw = name.split(' ');
            var parsedName = nameRaw[0].nameRaw[1];
            return parsedName;
        };

        /*
        open function for adding notes

        */
        $scope.notesData.addNote = function(row) {


            $scope.notesData.newNotes.push({
                id: $scope.notesData.newNotes.length + 1,
                rows: $scope.detailRows,
                date: new Date(),
                user: $rootScope.user

            });
        };

        // delete existing note
        $scope.notesData.deleteNote = function(note) {
            notesService.deleteNote(note._id, $rootScope.user).then(function(response) {
                $state.transitionTo($state.current.name, $stateParams);
                $scope.notesData.notes.splice($scope.notesData.notes.indexOf(note));
                $scope.alerts.push({
                    type: 'success',
                    msg: "Note deleted"
                });
                $scope.closeAlert(2000);
            });
        };
        $scope.$on('REMOVE_ROW', function(e, message) {
            $scope.notesData[message.array].splice(message.index, 1);

        });
        $scope.$on('ADD_NOTE', function(e, message) {
            var tempNotesObj = message.note;
            var idx = message.idx;
            var tempNoteRow = message.noteRow;
            notesService.writeNote(tempNotesObj).then(function(response) {
                $state.transitionTo($state.current.name, $stateParams);
                $scope.notesData.notes.push(response);
                $scope.alerts.push({
                    type: 'success',
                    msg: "Note successfully added"
                });
                $scope.closeAlert(2000);
            });

        });
        $scope.$on('UPDATE_NOTE', function(e, message) {
            $scope.alerts.push({
                type: 'success',
                msg: "Note successfully updated"
            });
            $scope.closeAlert(2000);
        });


    }
]);

/*


                    ##                          ##          ##
                    ##                          ##          ##
     ####    ###  #####   ###    ###      ### #####  # ##  ##
     ## ##  ## ##  ##    ## ##  ## ##    ## #  ##    ###   ##
     #  ## ##  ##  ##    #####   ##     ##     ##    ##    ##
    ## ##  ##  ## ##    ##        ##    ##    ##    ##    ##
    ## ##  ## ##  ##    ## ##  ## ##    ## ## ##    ##    ##
    ## ##   ###    ##    ###    ###      ###   ##   ##    ##


*/
app.controller('noteRowCtrl', ['$scope', '$rootScope', 'notesService', '$state', '$stateParams', '$timeout',
    function($scope, $rootScope, notesService, $state, $stateParams, $timeout) {

        $scope.annotation = null;
        $scope.currentNote = null;

        //save notes
        $scope.save = function(idx) {
            /*            $state.transitionTo($state.current.name, $stateParams);
             */
            if (!$scope.currentNote) {
                alert("fail");
                return;
            }
            //$scope.currentNote is currently a string -- it must be converted to a obj first
            $scope.currentNote = JSON.parse($scope.currentNote);

            tempNotesObj = {};
            tempNotesObj.rowData = $scope.currentNote;
            tempNotesObj.subID = $scope.currentNote.sub_id;
            tempNotesObj.activity = $scope.currentNote.activity_description;
            tempNotesObj.content = $scope.note.annotation;
            tempNotesObj.author = $scope.note.user.first_name + ' ' + $scope.note.user.last_name;

            tempNotesObj.safeID = $scope.note.user.safe_id;
            tempNotesObj.date = $scope.note.date;
            $scope.$emit('ADD_NOTE', {
                idx: idx,
                note: tempNotesObj,
                noteRow: $scope.note
            });
            $scope.notesData.newNotes.splice(idx, 1);
            /*            console.log("before",$state.current.name, $stateParams);
            $state.transitionTo($state.current.name, $stateParams);
            console.log("after",$state.current.name, $stateParams);*/
        };
        $scope.swapTasks = function(row) {
            if (row) {
                $scope.currentNote = row;
            }
        };
        $scope.rem = function(idx) {
            $scope.$emit('REMOVE_ROW', {
                array: 'newNotes',
                index: idx
            });
        };
        $scope.saveEdits = function(note) {

            note.active = false;
            notesService.updateNote(note, $rootScope.user).then(function(response) {
                $scope.$emit('UPDATE_NOTE', {

                });
            });
        };

    }
]);


/*



                           ##    ##                         ##          ##
                           ##                               ##          ##
      ####    ###   # ## #####  ##    ###   ####      ### #####  # ##  ##
      ## ##  #  ##  ###   ##    ##   ## #   ## ##    ## #  ##    ###   ##
      #  ##   ####  ##    ##    ##  ##      #  ##   ##     ##    ##    ##
     ##  #  ## ##  ##    ##    ##   ##     ##  #    ##    ##    ##    ##
     ## ##  ## ##  ##    ##    ##   ## ##  ## ##    ## ## ##    ##    ##
     ####    ## ## ##     ##   ##    ###   ####      ###   ##   ##    ##
    ##                                    ##
    ##                                    ##
*/

app.controller('participantCtrl', ['participantService', '$stateParams', '$scope',
    function(participantService, $stateParams, $scope) {
        participantNameFull = $stateParams.id;
        a = participantNameFull.split('_');
        participantNameFirst = a[0];
        participantNameLast = a[1];
        participantService.getParticipant(participantNameFirst, participantNameLast).then(function(response) {
            $scope.participantRows = response.rows;
        });
    }
]);


/*



      ##    ##                         ##            ##      ##
      ##    ##                         ##            ##      ##
    #####  ####   # ##   ###    ###   ####    ###   ##    ####    ###
     ##    ## ##  ###   ## ##  ## ##  ## ##  ## ##  ##   ## ##   ## ##
     ##    #  ##  ##    #####   ##    #  ## ##  ##  ##   #  ##    ##
    ##    ## ##  ##    ##        ##  ## ##  ##  ## ##   ##  #      ##
    ##    ## ##  ##    ## ##  ## ##  ## ##  ## ##  ##   ## ##   ## ##
     ##   ## ##  ##     ###    ###   ## ##   ###   ##    ####    ###


*/
/*THRESHOLDS VIEW START*/


app.controller('thresholdsCtrl', ['$scope', '$http', '$window', '$timeout', 'thresholdServices', 'tempLiveSearchService',
    function($scope, $http, $window, $timeout, thresholdServices, tempLiveSearchService) {
        $scope.processes = [];
        $scope.products = [];
        $scope.activities = [];
        $scope.transmitting = false;
        $scope.scaffoldLoading = false;
        $scope.errorMath = "";
        $scope.errorDataTypeGreen = "";
        $scope.errorDataTypeYellow = "";
        $scope.green_project = "";
        $scope.yellow_project = "";
        $scope.processSelect = [];
        $scope.treeDisplay = false;
        thresholdServices.getThresholds().success(function(data) {

            $scope.processes = data;

        });
        $scope.processSelectHandler = function() {
            $scope.products = $scope.processSelect.nodes;
        };
        $scope.productSelectHandler = function() {
            $scope.activities = $scope.productSelect.nodes;
        };
        $scope.activitySelectHandler = function() {
            var activity = $scope.activitySelect;
            if (activity.green_project) {
                $scope.green_project = activity.green_project;
                $scope.yellow_project = activity.yellow_project;
            } else {
                $scope.green_project = "";
                $scope.yellow_project = "";
            }

        };
        $scope.loadTree = function(grandParentLabel, parentLabel, activityLabel) {
            var procIdx = null;
            var prodIdx = null;
            var actIdx = null;
            for (var i = 0; i < $scope.processes.length; i++) {
                if (grandParentLabel === $scope.processes[i].label) {
                    $scope.treeDisplay = true;
                    $scope.processSelect = $scope.processes[i];
                    //$scope.productSelect = $scope.processSelect.nodes;
                    $scope.products = $scope.processSelect.nodes;
                    for (var j = 0; j < $scope.products.length; j++) {
                        if ($scope.products[j].label === parentLabel) {
                            $scope.productSelect = $scope.products[j];

                            //$scope.activitySelect = $scope.productSelect.nodes;
                            $scope.activities = $scope.productSelect.nodes;
                            for (var k = 0; k < $scope.activities.length; k++) {
                                if (activityLabel === $scope.activities[k].label) {
                                    $scope.activitySelect = $scope.activities[k];
                                    if ($scope.activitySelect.green_project) {
                                        $scope.green_project = $scope.activitySelect.green_project;

                                    } else {
                                        $scope.green_project = "";
                                    }
                                    if ($scope.activitySelect.yellow_project) {
                                        $scope.yellow_project = $scope.activitySelect.yellow_project;
                                    } else {
                                        $scope.yellow_project = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /*$scope.processSelect = $scope.processes[grandParentIndex];
            $scope.productSelect = $scope.products[parentIndex];
            $scope.activitySelect = $scope.activities[idx];*/
        };
        //new thresholds stuff
        $scope.makeThreshStructure = function() {
            $scope.scaffoldLoading = true;
            tempLiveSearchService.scaffoldThresholds().then(function(response) {

                thresholdServices.getThresholds().success(function(data) {

                    $scope.processes = data;
                    $scope.scaffoldLoading = false;
                    $scope.products = [];
                    $scope.activities = [];

                });
            });
        };
        $scope.submitThresholds = function() {
            var hasError = false;
            if ($scope.green_project > $scope.yellow_project) {
                $scope.errorMath = "The green threshold hold must be a lower number than the yellow threshold";
                $scope.green_project = "";
                $scope.yellow_project = "";
                hasError = true;
            }
            if (isNaN($scope.green_project * 1)) {
                $scope.errorDataTypeGreen = "error";
                hasError = true;
            }
            if (isNaN($scope.yellow_project * 1)) {
                $scope.errorDataTypeYellow = "error";
                hasError = true;
            }
            if (!$scope.processSelect.label || !$scope.productSelect.label || !$scope.activitySelect.label || !$scope.green_project || !$scope.yellow_project) {
                hasError = true;
            }
            if (hasError) {
                return false;
            } else {
                var threshObj = {};
                threshObj.process = $scope.processSelect.label;
                var tmpProcessObj = $scope.processSelect;
                threshObj.product = $scope.productSelect.label;
                var tmpProductObj = $scope.productSelect;
                threshObj.activity = $scope.activitySelect.label;
                var tmpActivityObj = $scope.activitySelect;
                threshObj.green = $scope.green_project;
                threshObj.yellow = $scope.yellow_project;
                $scope.transmitting = true;
                thresholdServices.setThreshold(threshObj).then(function(response) {
                    $scope.processes = response;

                    $scope.processSelect = tmpProcessObj;
                    $scope.productSelect = tmpProductObj;
                    $scope.activitySelect = tmpActivityObj;
                    $scope.green_project = threshObj.green;
                    $scope.yellow_project = threshObj.yellow;
                    $scope.transmitting = false;

                });
            }
        };


    }
]);

/*THRESHOLDS VIEW END*/

/*



                       ##                  ##          ##
                                           ##          ##
     # ## ##    ###   ##   ####      ### #####  # ##  ##
     ## ## ##  #  ##  ##   ## ##    ## #  ##    ###   ##
     ## ## ##   ####  ##   #  ##   ##     ##    ##    ##
    ## ## ##  ## ##  ##   ## ##    ##    ##    ##    ##
    ## ## ##  ## ##  ##   ## ##    ## ## ##    ##    ##
    ## ## ##   ## ## ##   ## ##     ###   ##   ##    ##


*/


app.controller('mainCtrl', function($scope, $rootScope, tempLiveSearchService, userService, tempListService, $modal, $http, $timeout, $state, $stateParams) {
    $scope.selected = undefined;
    $scope.showFilter = false;
    $scope.predicate = '-firm_name';
    $scope.projNumOutput = 5;
    $scope.actNumOutput = 5;
    $scope.projLabel = 'Late';
    $scope.actLabel = 'Late';
    $scope.activityPieHasData = true;
    $scope.totalActCount = {};
    $scope.totalProjCount = {};
    $scope.filterToggle = function() {
        $scope.showFilter = !$scope.showFilter;

    };
    $scope.rows = [];
    $scope.pieActivityCount = {};
    $scope.outputPieActivity = [];
    $scope.outputPieProject = [];
    $scope.pieProjectCount = {};
    $scope.activity = '';
    $scope.thresholdsObj = {};
    $scope.activityFilters = [];
    $scope.managerFilters = [];
    $scope.productFilters = [];
    $scope.processFilters = [];
    $scope.modifiedDate = '';
    $scope.hasUnassigned = false;
    $scope.actFilter = '';
    $scope.projFilter = '';
    $scope.selected = '';


    $scope.selectedOrg = capitalizeFirstLetter($state.current.name);
    $scope.storedActivityFilter = [];
    $scope.storedManagerFilter = [];
    $scope.storedProductFilter = [];
    $scope.storedProcessFilter = [];
    var updateTriggeredBy = "";
    $scope.projectPieChart = {

        'type': 'PieChart',
        'cssStyle': 'height:100%; width:100%;',
        'options': {
            'colors': ['#CC6666', '#ECD356', '#61B257', '#bababa'],
            'backgroundColor': {
                stroke: 'transparent',
                strokeWidth: 0,
                fill: 'transparent'
            },
            'chartArea': {
                top: '10%',
                left: '10%',
                width: '80%',
                height: '80%'
            },
            tooltip: {
                trigger: 'none'
            },
            'legend': 'none',
            'pieSliceText': 'value',
            'pieSliceBorderColor': "transparent",
            'pieSliceTextStyle': {
                fontName: "Oswald, sans-serif",
                color: '#fff',
                fontSize: 16
            }
        }
    };
    $scope.redProjectsArray = [{
        v: 'Late'
    }, {
        v: 10
    }];

    $scope.yellowProjectsArray = [{
        v: 'Caution'
    }, {
        v: 20
    }];
    $scope.greenProjectsArray = [{
        v: 'On Time'
    }, {
        v: 5
    }];
    $scope.greyProjectsArray = [{
        v: 'Untracked'
    }, {
        v: 55
    }];
    $scope.projectPieChart.data = {
        'cols': [{
            id: 't',
            label: 'Risk',
            type: 'string'
        }, {
            id: 's',
            label: 'Activities',
            type: 'number'
        }],
        'rows': [{
            c: $scope.redProjectsArray
        }, {
            c: $scope.yellowProjectsArray
        }, {
            c: $scope.greenProjectsArray
        }, {
            c: $scope.greyProjectsArray
        }]
    };



    $scope.activityPieChart = {
        'type': 'PieChart',
        'cssStyle': 'height:100%; width:100%;',
        'options': {
            'colors': ['#CC6666', '#ECD356', '#61B257'],
            'backgroundColor': {
                stroke: 'transparent',
                strokeWidth: 0,
                fill: 'transparent'
            },
            'chartArea': {
                top: '10%',
                left: '10%',
                width: '80%',
                height: '80%'
            },
            tooltip: {
                trigger: 'none'
            },
            'legend': 'none',
            'pieSliceText': 'value',
            'pieSliceBorderColor': "transparent",
            'pieSliceTextStyle': {
                fontName: "Oswald, sans-serif",
                color: '#fff',
                fontSize: 16
            }
        }
    };

    $scope.redActivitysArray = [{
        v: 'Late'
    }, {
        v: 10
    }];

    $scope.yellowActivitysArray = [{
        v: 'Caution'
    }, {
        v: 20
    }];
    $scope.greenActivitysArray = [{
        v: 'On Time'
    }, {
        v: 5
    }];


    $scope.activityPieChart.data = {
        'cols': [{
            id: 't',
            label: 'Risk',
            type: 'string'
        }, {
            id: 's',
            label: 'Activitys',
            type: 'number'
        }],
        'rows': [{
            c: $scope.redActivitysArray
        }, {
            c: $scope.yellowActivitysArray
        }, {
            c: $scope.greenActivitysArray
        }]
    };
    $scope.projSelectListener = function(selectedItem) {
        updateTriggeredBy = "colorSelection";
        /*$scope.actFilter = "";*/
        switch (selectedItem.row) {
            case 0:
                $scope.projFilter = "red";
                break;
            case 1:
                $scope.projFilter = "yellow";
                break;
            case 2:
                $scope.projFilter = "green";
                break;
            case 3:
                $scope.projFilter = "grey";
                break;
        }

    };
    $scope.actSelectListener = function(selectedItem) {
        updateTriggeredBy = "colorSelection";
        switch (selectedItem.row) {
            case 0:
                $scope.actFilter = "red";
                break;
            case 1:
                $scope.actFilter = "yellow";
                break;
            case 2:
                $scope.actFilter = "green";
                break;

        }

    };

    if ($rootScope.userData.pref_view) {
        if ($state.current.name === $rootScope.userData.pref_view.toLowerCase()) {
            if ($rootScope.userData.pref_act_filter) {
                $scope.storedActivityFilter = $rootScope.userData.pref_act_filter;
            } else {
                $scope.storedActivityFilter = [];
            }
            if ($rootScope.userData.pref_manager_filter) {
                $scope.storedManagerFilter = $rootScope.userData.pref_manager_filter;
            } else {
                $scope.storedManagerFilter = [];
            }
            if ($rootScope.userData.pref_product_filter) {
                $scope.storedProductFilter = $rootScope.userData.pref_product_filter;
            } else {
                $scope.storedProductFilter = [];
            }
            if ($rootScope.userData.pref_process_filter) {
                $scope.storedProcessFilter = $rootScope.userData.pref_process_filter;
            } else {
                $scope.storedProcessFilter = [];
            }
        }
    } else {
        $scope.storedActivityFilter = [];
        $scope.storedManagerFilter = [];
        $scope.storedProductFilter = [];
        $scope.storedProcessFilter = [];
    }
    if (sessionStorage.getItem($state.current.name + 'ActivityFilter')) {

        $scope.storedActivityFilter = JSON.parse(sessionStorage.getItem($state.current.name + 'ActivityFilter'));

    }
    if (sessionStorage.getItem($state.current.name + 'ManagerFilter')) {
        $scope.storedManagerFilter = JSON.parse(sessionStorage.getItem($state.current.name + 'ManagerFilter'));

    }
    if (sessionStorage.getItem($state.current.name + 'ProductFilter')) {
        $scope.storedProductFilter = JSON.parse(sessionStorage.getItem($state.current.name + 'ProductFilter'));

    }
    if (sessionStorage.getItem($state.current.name + 'ProcessFilter')) {
        $scope.storedProcessFilter = JSON.parse(sessionStorage.getItem($state.current.name + 'ProcessFilter'));
        console.log($scope.storedProcessFilter);


    }


    tempLiveSearchService.getData($state.current.name).then(function(response) {


        $scope.rows = response.rows;
        tempArrA = [];
        tempArrP = [];
        //$scope.thresholdsObj = response.thresholdsObj;
        $scope.managersList = response.managersList;
        $scope.untrackedData = response.untrackedData;
        $scope.activitiesList = response.activitiesList;
        //$scope.totalActCount = response.threshActTotals;
        //$scope.totalProjCount = response.threshProjTotals;
        $scope.productsList = response.productsList;
        $scope.processList = response.processList;



        $scope.activityFilters = [];


        $scope.projColorFilter = 'red';
        $scope.actColorFilter = 'red';



        $scope.activityFilters = _.intersection($scope.storedActivityFilter, $scope.activitiesList);
        $scope.managerFilters = _.intersection($scope.storedManagerFilter, $scope.managersList);
        $scope.productFilters = _.intersection($scope.storedProductFilter, $scope.productsList);
        $scope.processFilters = _.intersection($scope.storedProcessFilter, $scope.processesList);
        console.log("process filter", $scope.processFilters);

    });

    $scope.$watchCollection('filteredRows', function(changed) {

        var agreen = 0;
        var ayellow = 0;
        var ared = 0;
        var pgreen = 0;
        var pyellow = 0;
        var pred = 0;
        var pgrey = 0;

        if (!$scope.filteredRows) {
            return;
        }
        for (var i = 0; i < $scope.filteredRows.length; i++) {

            switch ($scope.filteredRows[i].activity_status) {
                case "Green":
                    agreen++;
                    break;
                case "Yellow":
                    ayellow++;
                    break;
                case "Red":
                    ared++;
                    if ($scope.filteredRows[i].participant_last_name === "Unassigned") {
                        $scope.hasUnassigned = true;
                    }
                    break;
            }
            switch ($scope.filteredRows[i].project_color) {
                case "Green":
                    pgreen++;
                    break;
                case "Yellow":
                    pyellow++;
                    break;
                case "Red":
                    pred++;
                    if ($scope.filteredRows[i].participant_last_name === "Unassigned") {
                        $scope.hasUnassigned = true;
                    }
                    break;
                case "Grey":
                    pgrey++;
                    break;
                default:
                    pgrey++;
                    break;
            }

        }
        if ($scope.totalProjCount) {
            $scope.totalProjCount.totalProjRedCount = pred;
            $scope.totalProjCount.totalProjYellowCount = pyellow;
            $scope.totalProjCount.totalProjGreenCount = pgreen;
            $scope.totalProjCount.totalProjGreyCount = pgrey;
        }
        if ($scope.totalActCount) {
            $scope.totalActCount.totalActRedCount = ared;
            $scope.totalActCount.totalActYellowCount = ayellow;
            $scope.totalActCount.totalActGreenCount = agreen;
        }



        if (updateTriggeredBy === "colorSelection") {
            updateTriggeredBy = "";
            return;
        }
        $scope.redProjectsArray[1].v = pred;
        $scope.yellowProjectsArray[1].v = pyellow;
        $scope.greenProjectsArray[1].v = pgreen;
        $scope.greyProjectsArray[1].v = pgrey;

        $scope.redActivitysArray[1].v = ared;
        $scope.yellowActivitysArray[1].v = ayellow;
        $scope.greenActivitysArray[1].v = agreen;



    });

    $scope.updateProcessFilters = function(theProcess, idx) {



        if (theProcess !== 'none') {
            if (!_.contains($scope.storedProcessFilter, theProcess)) {
                $scope.storedProcessFilter.push(theProcess);

            } else {
                for (var i = 0; i < $scope.storedProcessFilter.length; i++) {
                    if ($scope.storedProcessFilter[i] === theProcess) {
                        $scope.storedProcessFilter.splice(i, 1);

                        break;
                    }
                }
            }
        } else {
            $scope.storedProcessFilter = [];


        }
        sessionStorage.setItem($state.current.name + "ProcessFilter", JSON.stringify($scope.storedProcessFilter));

    };

    //remove current manager filters
    $scope.removeProcessFilters = function(item) {

        var idx = $scope.storedProcessFilter.indexOf(item);
        $scope.storedProcessFilter.splice(idx, 1);
        sessionStorage.setItem($state.current.name + "ProcessFilter", JSON.stringify($scope.storedProcessFilter));


    };
    $scope.updateActivityFilters = function(theActivity) {

        //sessionStorage.setItem($state.current.name + "ActivityFilter", theActivity);
        if (theActivity !== 'none') {
            if (!_.contains($scope.storedActivityFilter, theActivity)) {
                $scope.storedActivityFilter.push(theActivity);
                /*                $scope.filterDisplay(theActivity);
                 */
            } else {
                for (var i = 0; i < $scope.storedActivityFilter.length; i++) {
                    if ($scope.storedActivityFilter[i] === theActivity) {
                        $scope.storedActivityFilter.splice(i, 1);

                        break;
                    }
                }
            }

        } else {
            $scope.storedActivityFilter = [];


        }
        sessionStorage.setItem($state.current.name + "ActivityFilter", JSON.stringify($scope.storedActivityFilter));

    };
    //remove current activity filters
    $scope.removeActivityFilters = function(item) {

        var idx = $scope.storedActivityFilter.indexOf(item);
        $scope.storedActivityFilter.splice(idx, 1);
        sessionStorage.setItem($state.current.name + "ActivityFilter", JSON.stringify($scope.storedActivityFilter));

    };


    $scope.updateManagerFilters = function(theManager, idx) {



        //sessionStorage.setItem($state.current.name + "ManagerFilter", theManager);
        if (theManager !== 'none') {
            if (!_.contains($scope.storedManagerFilter, theManager)) {
                $scope.storedManagerFilter.push(theManager);

            } else {
                for (var i = 0; i < $scope.storedManagerFilter.length; i++) {
                    if ($scope.storedManagerFilter[i] === theManager) {
                        $scope.storedManagerFilter.splice(i, 1);

                        break;
                    }
                }
            }

            //$scope.storedManagerFilter = theManager;
        } else {
            $scope.storedManagerFilter = [];

            //$scope.storedManagerFilter = "";
        }
        sessionStorage.setItem($state.current.name + "ManagerFilter", JSON.stringify($scope.storedManagerFilter));

    };

    $scope.removeManagerFilters = function(item) {

        var idx = $scope.storedManagerFilter.indexOf(item);
        $scope.storedManagerFilter.splice(idx, 1);
        sessionStorage.setItem($state.current.name + "ManagerFilter", JSON.stringify($scope.storedManagerFilter));

    };

    $scope.updateProductFilters = function(theProduct, idx) {
        // will reimplement when all multi-selects (checkboxes) are in place
        /*console.log(theProduct);

        sessionStorage.setItem($state.current.name + "ProductFilter", theProduct);
        if (theProduct !== "none") {
            $scope.productFilters = theProduct;
            $scope.storedProductFilter = theProduct;
        } else {

            $scope.productFilters = "";
            $scope.storedProductFilter = "";
        }*/
        if (theProduct !== 'none') {
            if (!_.contains($scope.storedProductFilter, theProduct)) {
                $scope.storedProductFilter.push(theProduct);

            } else {
                for (var i = 0; i < $scope.storedProductFilter.length; i++) {
                    if ($scope.storedProductFilter[i] === theProduct) {
                        $scope.storedProductFilter.splice(i, 1);

                        break;
                    }
                }
            }

        } else {

            $scope.storedProductFilter = [];


        }
        sessionStorage.setItem($state.current.name + 'ProductFilter', JSON.stringify($scope.storedProductFilter));

    };

    //remove current product filters
    $scope.removeProductFilters = function(item) {

        var idx = $scope.storedProductFilter.indexOf(item);
        $scope.storedProductFilter.splice(idx, 1);
        sessionStorage.setItem($state.current.name + 'ProductFilter', JSON.stringify($scope.storedProductFilter));

    };

    //function to show/hide the Show All button for current filter status. Counts up characters currently being displayed in the current filters
    /*$scope.currentFilterSize = function(){
        $scope.filterStatus = angular.element(document).find('.filter-status');
        if($scope.filterStatus.height() >= 36){
            $scope.currentStatusLong = true;
        }
}*/


    $scope.filterProcess = function(item) {

        if ($scope.storedProcessFilter.length < 1) {
            return true;
        }
        if (_.contains($scope.storedProcessFilter, item.process_description)) {

            return true;

        } else {

            return false;
        }
    };
    $scope.filterManagers = function(item) {
        if ($scope.storedManagerFilter.length < 1) {
            return true;
        }
        if (_.contains($scope.storedManagerFilter, item.staff_manager)) {

            return true;

        } else {

            return false;
        }
    };


    $scope.filterProducts = function(item) {

        if ($scope.storedProductFilter.length < 1) {
            return true;
        }
        if (_.contains($scope.storedProductFilter, item.short_product)) {

            return true;

        } else {

            return false;
        }
    };
    $scope.filterActivities = function(item) {

        if ($scope.storedActivityFilter.length < 1) {
            return true;
        }
        if (_.contains($scope.storedActivityFilter, item.activity_description)) {

            return true;

        } else {
            console.log('filtered');
            return false;
        }
    };
    /*TEMPORARY CLICK TO REFRESH BUTTON WITH ALERT*/
    $scope.alerts = [];
    $scope.closeAlert = function(timeout) {

        $timeout(function() {
            $scope.alerts = [];
        }, timeout);
    };

    $scope.updateData = function() {
        /* $scope.loading = true;
        $scope.warningMsg = false;
        $http.get('/update_data').success(function(data) {
            var currentOrg = $state.current.name;
            var currentOrgParsed = capitalizeFirstLetter(currentOrg);
            if (data === 'Whoops! Data was not updated!') {
                $scope.alerts.push({
                    type: 'danger',
                    msg: data
                });
                $scope.loading = false;
                $scope.warningMsg = true;

            } else {
                $scope.alerts.push({
                    type: 'success',
                    msg: data
                });
                sharedProperties.getRows(currentOrgParsed).then(function(response) {
                    // table data comes from $scope.rows
                    $scope.rows = response.rows;
                });
                $scope.loading = false;
                $scope.closeAlert(2000);
            }


        });*/
    };


    $scope.focusFilter = function(actColor, projColor) {
        var returnResult = '';
        $scope.proColorFilter = '';
        $scope.actColorFilter = '';
        switch ($scope.filterBy) {

            case 'activity':
                actColorFilter = actColor;

                break;
            case 'activity':
                proColorFilter = projColor;

                break;
            default:

                //umm, well this is embarassing...
                break;
        }
        return returnResult;


    };
    $scope.clearColorFilters = function(ageType) {

        if (ageType === "project") {
            $scope.projFilter = "";
            $scope.filterBy = "";
        } else if (ageType === "activity") {
            $scope.actFilter = "";
            $scope.filterBy = "";
        } else {
            //console.log("unexpected age type");
        }

    };
    $scope.currentFilters = [];

    //display current filters
    $scope.filterDisplay = function(item) {
        $scope.currentFilters.push(item);
    };

    //remove current product filters
    $scope.removefilterDisplay = function(item) {

        var idx = $scope.currentFilters.indexOf(item);
        $scope.currentFilters.splice(idx, 1);

    };



    $scope.openUnassigned = function() {
        var modalInstance = $modal.open({
            templateUrl: 'static/partials/unassigned.html',
            controller: unassignedCtrl,
            resolve: {
                rows: function() {
                    return $scope.rows;
                }
            }
        });

    };


    $scope.cancel = function() {
        $modalInstance.close('cancel');
    };

});


var unassignedCtrl = function($scope, $modalInstance, $filter, rows) {
    $scope.rows = rows;
    $scope.unassignedFilter = function(row) {
        return row.participant_last_name === 'Unassigned' && row.activity_status === 'Red' || row.participant_last_name === 'Unassigned' && row.activity_color === 'red';
    };

    $scope.ok = function() {
        $modalInstance.close();

    };
    $modalInstance.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

};
/*



                    ##    ##    ##
                    ##    ##
      ###    ###  ##### #####  ##   ####    ####   ###
     ## ##  ## ##  ##    ##    ##   ## ##  ## ##  ## ##
      ##    #####  ##    ##    ##   #  ##  #  ##   ##
       ##  ##     ##    ##    ##   ## ##  ##  #     ##
    ## ##  ## ##  ##    ##    ##   ## ##  ## ##  ## ##
     ###    ###    ##    ##   ##   ## ##   ####   ###
                                            ##
                                          ###



                          ##          ##
                          ##          ##
     # ## ##    ###    ####    ###   ##
     ## ## ##  ## ##  ## ##   #  ##  ##
     ## ## ## ##  ##  #  ##    ####  ##
    ## ## ##  ##  ## ##  #   ## ##  ##
    ## ## ##  ## ##  ## ##   ## ##  ##
    ## ## ##   ###    ####    ## ## ##


*/
var settingsCtrl = function($scope, $rootScope, $modalInstance, user, userData, tempListService, userService, $state, $stateParams) {

    $scope.user = user;
    $scope.userData = $rootScope.userData;
    $scope.settingsOrg = null;
    $scope.orgManagers = [];
    $scope.orgActivities = [];
    $scope.orgProcesses = [];
    $scope.orgProducts = [];
    $scope.setFlag = false;
    $scope.orgChoices = [

        {
            'name': 'Design',
            'value': 'Design'
        }, {
            'name': 'Development',
            'value': 'Development'
        }

    ];

    // if there is a pref view - set it
    if ($rootScope.userData.pref_view !== '' || $rootScope.userData.pref_view !== undefined) {
        for (var i = 0; i < $scope.orgChoices.length; i++) {

            if ($rootScope.userData.pref_view === $scope.orgChoices[i].name) {
                $scope.settingsOrg = $scope.orgChoices[i];
                $scope.setFlag = true;
                break;
            }
        }
    }

    $scope.listsLoaded = false;
    var lists = [];
    var checkLists = function() {

        if (lists.length >= 2) {
            $scope.listsLoaded = true;


        }

        if ($rootScope.userData.pref_process_filter) {
            $scope.settingsProcess = $rootScope.userData.pref_process_filter;
        } else {
            $scope.settingsProcess = [];
        }

        if ($rootScope.userData.pref_product_filter) {
            $scope.settingsProduct = $rootScope.userData.pref_product_filter;
        } else {
            $scope.settingsProduct = [];
        }

        if ($rootScope.userData.pref_act_filter) {
            //normalize left over string filters from previous build
            if (typeof $rootScope.userData.pref_act_filter === 'string') {
                $scope.settingsActivity = [];
            } else {
                $scope.settingsActivity = $rootScope.userData.pref_act_filter;
            }
        } else {
            $scope.settingsActivity = [];
        }

        if ($rootScope.userData.pref_manager_filter) {
            //normalize left over string filters from previous build
            if (typeof $rootScope.userData.pref_manager_filter === 'string') {
                $scope.settingsManager = [];
            } else {
                $scope.settingsManager = $rootScope.userData.pref_manager_filter;
            }
            $scope.settingsManager = $rootScope.userData.pref_manager_filter;
        } else {
            $scope.settingsManager = [];
        }

    };


    $scope.setLists = function() {
        lists = [];
        $scope.orgProcesses = [];
        $scope.orgProducts = [];
        $scope.orgManagers = [];
        $scope.orgActivities = [];
        //using this because of the child scope
        console.log('This is', this);
        tempListService.getList('process', this.settingsOrg.name).then(function(response) {
            $scope.orgProcesses = response.data;
            lists.push(response.data.list);
            checkLists();
        });
        tempListService.getList('product', this.settingsOrg.name).then(function(response) {
            $scope.orgProducts = response.data;
            lists.push(response.data.list);
            checkLists();
        });
        tempListService.getList('activity', this.settingsOrg.name).then(function(response) {
            $scope.orgActivities = response.data;
            lists.push(response.data.list);
            checkLists();
        });
        tempListService.getList('manager', this.settingsOrg.name).then(function(response) {
            $scope.orgManagers = response.data;
            lists.push(response.data);
            checkLists();

        });
    };

    if ($rootScope.userData.pref_view) {
        $scope.listsLoaded = true;
        $scope.setLists();
    }

    //select/deselect user prefs
    $scope.toggleSelection = function toggleSelection(filterCategory, item) {
        var idx = $scope[filterCategory].indexOf(item);
        if (idx > -1) {
            //normalize left over string filters from previous build
            if (typeof $scope[filterCategory] === 'string') {
                $scope[filterCategory] = [];
            }
            $scope[filterCategory].splice(idx, 1);
            console.log($scope[filterCategory]);
        } else {
            //normalize left over string filters from previous build
            if (typeof $scope[filterCategory] === 'string') {
                $scope[filterCategory] = [];
            }
            $scope[filterCategory].push(item);
            console.log($scope[filterCategory]);
        }
    };



    $scope.ok = function() {

        var formData = {
            'org': this.settingsOrg.value,
            'activity': $scope.settingsActivity,
            'product': $scope.settingsProduct,
            'process': $scope.settingsProcess,
            'manager': $scope.settingsManager
        };

        userService.setUserData(formData).then(function(response) {
            console.log("YIPPEEEE",response);
            $rootScope.userData = response;



            $modalInstance.close();
            sessionStorage.setItem($rootScope.userData.pref_view.toLowerCase() + "ProcessFilter", JSON.stringify($scope.settingsActivity));
            sessionStorage.setItem($rootScope.userData.pref_view.toLowerCase() + "ProductFilter", JSON.stringify($scope.settingsProduct));
            sessionStorage.setItem($rootScope.userData.pref_view.toLowerCase() + "ActivityFilter", JSON.stringify($scope.settingsProcess));
            sessionStorage.setItem($rootScope.userData.pref_view.toLowerCase() + "ManagerFilter", JSON.stringify($scope.settingsManager));
            if($rootScope.userData.pref_view === 0){
                $state.go('design');
            } else {
                $state.go($rootScope.userData.pref_view.toLowerCase(), $stateParams, {
                    reload: true
                });
            }

        });


    };
    //close modal without commiting any changes
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

    //resets user prefs
    $scope.resetAll = function() {
        //remove all preferences
        userService.resetUserPrefs($rootScope.user).then(function(response) {
            $rootScope.userData = response;
            $scope.userData = response;
            $scope.setFlag = false;
            $scope.settingsOrg = {
                'name': '--Select One --',
                'value': 0
            };
            $scope.settingsActivity = [];
            $scope.settingsProduct = [];
            $scope.settingsProcess = [];
            $scope.settingsManager = [];
            $scope.listsLoaded = false;
            lists = [];
        });

    };

};



/*



                    ##                         ##          ##
                    ##                         ##          ##
      ###   ## ## #####   ###   # ##     ### #####  # ##  ##
     ## ##  ## ##  ##    ## ##  ###     ## #  ##    ###   ##
    ##  ## ##  ##  ##    #####  ##     ##     ##    ##    ##
    ##  ## ## ##  ##    ##     ##      ##    ##    ##    ##
    ## ##  ## ##  ##    ## ##  ##      ## ## ##    ##    ##
     ###    ## #   ##    ###   ##       ###   ##   ##    ##


*/

var outerCtrl = app.controller('outerCtrl', function($modal, $scope, $rootScope, $stateParams, userService, tempListService, tempLiveSearchService, $state, notesService) {

    $scope.user = {};
    $scope.userData = {};
    $scope.pref = {};
    $rootScope.notes = {};
    notesService.readNotes().then(function(response) {
        $rootScope.notes = response;
    });


    userService.getUser().then(function(response) {

        $rootScope.user = response;


    });

    tempLiveSearchService.getDateModified().success(function(response) {
        modifiedDate = new Date(response * 1000);

        $scope.modifiedDate = modifiedDate;

    });

    //Settings Modal
    $scope.openSettings = function() {

        $scope.loading = true;

        userService.getUserData().then(function(response) {
            $rootScope.userData = response;

            $scope.loading = false;
            var modalInstance = $modal.open({
                templateUrl: 'static/partials/settings.html',
                controller: settingsCtrl,
                resolve: {
                    userData: function() {
                        return $scope.userData;
                    },
                    user: function() {
                        return $rootScope.user;
                    }

                }
            });
        });

    };

    //Set previous state in session storage
    $rootScope.$on('$stateChangeStart',
        function(event) {
            paramsString = JSON.stringify($stateParams);
            sessionStorage.setItem("previousState", $state.current.name);

            sessionStorage.setItem("previousStateParams", paramsString);


        });

    // set current state in session storage for page refreshes
    $rootScope.$on('$stateChangeSuccess',
        function(event) {
            paramsString = JSON.stringify($stateParams);
            sessionStorage.setItem('lastViewed', $state.current.name);

            sessionStorage.setItem('lastViewedParams', paramsString);


        });


    // get the past state from session storage to enable return button functionality
    $scope.previousState = function() {
        pastState = sessionStorage.getItem("previousState");
        if (sessionStorage.getItem("previousStateParams") !== "{}") {
            pastStateParam = JSON.parse(sessionStorage.getItem("previousStateParams"));
            //grab value of id prop for display in template
            pastStateParamRaw = pastStateParam.id;
        } else {
            pastStateParam = '';
        }
        $state.go(pastState, pastStateParam);
    };

    //Hide feedback msg
    if (sessionStorage.getItem('feedbackMsg') === 'no') {
        $scope.feedbackAlert = false;
    } else if (sessionStorage.getItem('feedbackMsg') === 'yes') {
        $scope.feedbackAlert = true;
    }
    $scope.hideFeedback = function() {
        var saveFeedbackStatus = sessionStorage.setItem('feedbackMsg', 'no');
        $scope.feedbackAlert = false;
    };

    $scope.showFeedback = function() {
        var saveFeedbackStatus = sessionStorage.setItem('feedbackMsg', 'yes');
        $scope.feedbackAlert = true;
    };


});

//Add fields to detail view
var addFieldCtrl = function($scope, $modalInstance, $filter, rows) {
    $scope.rows = rows;
    //Grab all keys from rows and create an array
    $scope.keyList = [];
    angular.forEach($scope.rows, function(value, key) {
        $scope.keyList.push(key);
    });

    if ($scope.rows.selected) {
        $scope.selection = $scope.rows.selected;
    } else {
        $scope.selection = [];
    }

    $scope.ok = function() {
        $modalInstance.close($scope.selection);

    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };


};