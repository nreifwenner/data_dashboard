/*var parseThresholds = function(thresholdsData) {

    var thresholdsObj = {};
    for (var i = 0; i < thresholdsData.length; i++) {

        tActivity = thresholdsData[i].activity;
        tGreenActivity = thresholdsData[i].green_activity;
        tYellowActivity = thresholdsData[i].yellow_activity;
        tGreenProject = thresholdsData[i].green_project;
        tYellowProject = thresholdsData[i].yellow_project;
        thresholdsObj[tActivity] = {
            activityGreen: tGreenActivity,
            activityYellow: tYellowActivity,
            projectGreen: tGreenProject,
            projectYellow: tYellowProject,
            activityGreenCount: 0,
            activityYellowCount: 0,
            activityRedCount: 0,
            projectGreenCount: 0,
            projectYellowCount: 0,
            projectRedCount: 0
        };

    }
    return thresholdsObj;
};*/
var convertOrgUnit = function(org) {

    switch (org) {
        case "Development":
            convertedOrgUnit = "FED BGL+FED EGN";
            break;

        default:
            convertedOrgUnit = org;
            break;

    }

    return convertedOrgUnit;
};
var parseRowsAssignColors = function(data, thresholdsObj, notesObject) {
    activitiesList = [];
    managersList = [];
    processList = [];

    newData = [];
    totalActivityRedCount = 0;
    totalActivityYellowCount = 0;
    totalActivityGreenCount = 0;
    totalProjectRedCount = 0;
    totalProjectYellowCount = 0;
    totalProjectGreenCount = 0;
    totalProjectGreyCount = 0;
    untrackedData = false;
    //building process hierarchy

    productArr = [];

    for (i = 0; i < data.length; i++) {

        //create a list of all the activities on the row data
        if (!_.contains(activitiesList, data[i].activity_description)) {
            activitiesList.push(data[i].activity_description);
        }
        if (!_.contains(managersList, data[i].staff_manager)) {
            managersList.push(data[i].staff_manager);
        }
        if (!_.contains(processList, data[i].process_description)) {
            processList.push(data[i].process_description);
        }
        var rawProduct = data[i].product;
        var tmpPrArr;
        //splitting at colon
        tmpPrArr = rawProduct.split(":");
        var product = tmpPrArr[0];
        data[i].short_product = product;

        if (!_.contains(productArr, product)) {
            productArr.push(product);
        }



        var foundProject = false;
        //////
        ///threshold schema
        // {
        //  label:"ProcessName",
        //  nodes:[
        //      {
        //          label:"Product Name",
        //          nodes:[
        //              {
        //                  label:"Activity Description",
        //                  green_project: number,
        //                  yellow_project: number
        //              }
        //              
        //          ]
        //      }
        //  ]
        // }
        ////////

        //TODO: make recursive function for this

        //process level
        for (var k = 0; k < thresholdsObj.length; k++) {

            if (thresholdsObj[k].label === data[i].process_description) {
                //product level
                for (var l = 0; l < thresholdsObj[k].nodes.length; l++) {
                    if (thresholdsObj[k].nodes[l].label === data[i].product) {
                        console.log(thresholdsObj[k].nodes[l].label);
                        //activity level
                        for (var m = 0; m < thresholdsObj[k].nodes[l].nodes.length; m++) {
                            if (thresholdsObj[k].nodes[l].nodes[m].label === data[i].activity_description) {
                                console.log(thresholdsObj[k].nodes[l].nodes[m].label);
                                if (thresholdsObj[k].nodes[l].nodes[m].green_project > 0 && thresholdsObj[k].nodes[l].nodes[m].yellow_project) {
                                    if (data[i].project_age <= thresholdsObj[k].nodes[l].nodes[m].green_project) {
                                        data[i].project_color = "Green";


                                    } else if (data[i].project_age > thresholdsObj[k].nodes[l].nodes[m].green_project && data[i].project_age <= thresholdsObj[k].nodes[l].nodes[m].yellow_project) {
                                        data[i].project_color = "Yellow";

                                    } else {
                                        data[i].project_color = "Red";

                                    }
                                    foundProject = true;
                                }


                            }
                        }
                    }
                }
            }


        }


        if (!foundProject) {
            data[i].project_color = "Grey";

            untrackedData = true;
        }


        data[i].notes = [];
        for (j = 0; j < notesObject.length; j++) {

            if (data[i].sub_id === notesObject[j].subID) {
                data[i].notes.push(notesObject[j]);
                //console.log(data[i]);
            }
        }
        newData.push(data[i]);



    }
    return {
        thresholdsObj: thresholdsObj,
        processList: processList,
        activitiesList: activitiesList,
        managersList: managersList,
        untrackedData: untrackedData,
        productsList: productArr,
        threshActTotals: {
            'totalActRedCount': totalActivityRedCount,
            'totalActYellowCount': totalActivityYellowCount,
            'totalActGreenCount': totalActivityGreenCount
        },
        threshProjTotals: {
            'totalProjRedCount': totalProjectRedCount,
            'totalProjYellowCount': totalProjectYellowCount,
            'totalProjGreenCount': totalProjectGreenCount,
            'totalProjGreyCount': totalProjectGreyCount
        },
        rows: newData
    };

};
focusFilter = function(actColor, projColor) {

    var returnResult = true;
    switch ($scope.filterBy) {

        case "activity":
            returnResult = (actColor === $scope.actColorFilter) ? true : false;
            ////console.log("activity" + actColor + "|" + $scope.actColorFilter + "|" + returnResult);

            break;
        case "project":
            returnResult = (projColor === $scope.projColorFilter) ? true : false;

            break;
        default:
            ////console.log("none");

            break;
    }
    return returnResult;


};

var transformServiceRows = function(rowArray, org) {

    var args = arguments;
    var newRows = [];
    if (org) {
        org = org.toLowerCase();
    }

    _.each(rowArray, function(project) {



        //logic to get the right activity
        if (org === 'development') {
            devLoop: for (var i = 0; i < project.activities.length; i++) {
                if (project.activities[i].Org_Unit === 'FED EGN' || project.activities[i].Org_Unit === 'FED BGL') {
                    newRows.push(makeRow(project, project.activities[i]));
                } else {

                }


            }
        } else if (org === null) {
            console.log("org is null");
            console.log(args[2]);
            if (args[2] && args[3]) {
                var first_name = args[2];
                var last_name = args[3];

                participantLoop: for (var l = 0; l < project.activities.length; l++) {
                    if (project.activities[l].First_Name === first_name && project.activities[l].Last_Name === last_name) {
                        newRows.push(makeRow(project, project.activities[l]));
                    }

                }
            } else {
                nullLoop: for (var k = 0; k < project.activities.length; k++) {
                    newRows.push(makeRow(project, project.activities[k]));
                }
            }

        } else {
            otherLoop: for (var j = 0; j < project.activities.length; j++) {
                if (project.activities[j].Org_Unit.toLowerCase() === org.toLowerCase()) {
                    newRows.push(makeRow(project, project.activities[j]));
                } else {
                    // something else
                }

            }
        }



    });
    return newRows;
};

function makeRow(project, projectActivity) {
    var tempRow = {};
    if (projectActivity.Last_Name === "") {
        tempRow.participant_last_name = "Unassigned";
    } else {
        tempRow.participant_last_name = projectActivity.Last_Name;
    }
    if (projectActivity.Staff_Manager === "") {
        tempRow.staff_manager = "Unassigned";
    } else {
        tempRow.staff_manager = projectActivity.Staff_Manager;

    }
    tempRow.participant_first_name = projectActivity.First_Name;
    tempRow.org_unit = projectActivity.Org_Unit;
    tempRow.activity_description = projectActivity.Activity_Description;
    tempRow.activity_age = projectActivity.Activity_Age_In_Business_Days * 1;
    tempRow.activity_due = projectActivity.Activity_Due_Date;
    tempRow.project_age = project["#Project_Age_In_Days"] * 1;
    tempRow.pm_name = project.Project_Manager;
    tempRow.firm_name = project.Firm_Name;
    tempRow.wld_id = project.Wld_Id * 1;
    tempRow.sub_id = project.Sub_Id * 1;
    tempRow.logical_site = project.Logical_Site_Id * 1;
    tempRow.sap_type = project.Sap_Account_Type;
    if (project.Account_Value === "") {
        tempRow.Account_Value = "0.00";
    } else {
        tempRow.account_value = parseFloat(project.Account_Value);
    }

    tempRow.product = project.Product;
    tempRow.process_description = project.Process_Description;

    tempRow.completed_date = project.Completed_Date;
    tempRow.status = project.Status;
    tempRow.bpm_id = project.Bpm_Project_Id * 1;
    tempRow.cdc = project.Cdc;
    tempRow.rsm = project.Rsm;
    tempRow.writer = project.Writer;
    tempRow.designer = project.Designer;
    tempRow.seo = project.Seo;
    tempRow.line_item_status = project.Line_Item_Status;
    tempRow.activity_status = project.activities[0].Activity_Status;
    tempRow.project_status = project.Project_Status;
    tempRow.priority = project.Priority;
    return tempRow;
}

function makeThresholdScaffolding(rows) {
    var threshArr = [];
    _.each(rows, function(project) {
        for (var m = 0; m < project.activities.length; m++) {
            var tempThresh = {};
            tempThresh.activity = project.activities[m].Activity_Description;
            tmpPrArr = project.Product.split(':');

            var trimmedProduct = tmpPrArr[0];
            tempThresh.product = trimmedProduct;
            tempThresh.process = project.Process_Description;
            threshArr.push(tempThresh);
        }
    });
    return threshArr;

}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}