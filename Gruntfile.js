module.exports = function(grunt) {

  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.initConfig({

    notify: {
      uglify: {
        options: {
          title: 'Made Ugly!', // optional
          message: 'Uglify finished running', //required
        }
      },

      watch:{
        livereload: {
              options: {
                title: 'Task Complete', // optional
                message: 'Page succesfully reloaded!', //required
              }
            }
          },
      flask: {
        options: {
          message: 'Flask Server is ready!'
        }
      }
    },

    jshint: {
      options: {

        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        },
      },
      all: ['data_dashboard/static/src/utility-functions.js', 'data_dashboard/static/src/app.js', 'data_dashboard/static/src/services.js', 'data_dashboard/static/src/directives.js']
    },

    uglify: {
      dev: {
        options: {
          mangle: false,
          compress: true,
          sourceMap: function(path) {
            return path + ".map";
          },
          preserveComments: 'all',
          beautify: true
        },
        files: {
          'data_dashboard/static/js/traf.min.js': ['data_dashboard/static/src/lodash.min.js', 'data_dashboard/static/src/affix.js', 'data_dashboard/static/src/ui-bootstrap-tpls-0.10.0.min.js', 'data_dashboard/static/src/angular-ui-router.js', 'data_dashboard/static/src/ng-google-chart.js', 'data_dashboard/static/src/utility-functions.js', 'data_dashboard/static/src/app.js', 'data_dashboard/static/src/services.js', 'data_dashboard/static/src/directives.js']
        }
      },
      dist: {
        options: {
          mangle: false,
          compress: true,
          sourceMap: function(path) {
            return path + ".map";
          }

        },
        files: {
          'data_dashboard/static/js/traf.min.js': ['data_dashboard/static/src/lodash.min.js', 'data_dashboard/static/src/affix.js', 'data_dashboard/static/src/ui-bootstrap-tpls-0.10.0.min.js', 'data_dashboard/static/src/angular-ui-router.js', 'data_dashboard/static/src/ng-google-chart.js', 'data_dashboard/static/src/utility-functions.js', 'data_dashboard/static/src/app.js', 'data_dashboard/static/src/services.js', 'data_dashboard/static/src/directives.js']
        }
      }
    },

    compass: {
      dev: {
        options: {
          config: 'config.rb',
          force: true
        }
      }
    },

    watch: {

      js: {
        files: ['data_dashboard/static/src/lodash.min.js', 'data_dashboard/static/src/affix.js', 'data_dashboard/static/src/ui-bootstrap-tpls-0.10.0.min.js', 'data_dashboard/static/src/angular-ui-router.js', 'data_dashboard/static/src/ng-google-chart.js', 'data_dashboard/static/src/utility-functions.js', 'data_dashboard/static/src/app.js', 'data_dashboard/static/src/services.js', 'data_dashboard/static/src/directives.js'],
        tasks: ['jshint', 'uglify:dev']
      },

      sass: {
        files: ['sass/**/*.scss'],
        tasks: ['compass:dev']
      },

      /* watch our files for change, reload */
      livereload: {
        files: ['data_dashboard/static/**/*.html', 'data_dashboard/static/**/*.css', 'data_dashboard/static/images/**', 'data_dashboard/static/**/*.js'],
        options: {
          livereload: true
        }
      }
    }


  });
  // New task for flask server
  grunt.registerTask('flask', 'Run flask server.', function() {
    var spawn = require('child_process').spawn;
    grunt.log.writeln('Starting Flask development server.');
    // stdio: 'inherit' let us see flask output in grunt
    var PIPE = {
      stdio: 'inherit'
    };
    spawn('python', ['runserver.py'], PIPE);

  });


  grunt.registerTask('default', 'watch');
  grunt.registerTask('little', ['uglify:dist']);
  grunt.registerTask('server', function() {
    grunt.task.run([
      'jshint',
      'uglify:dist',
      'flask',
      'notify:flask',
      'watch'
    ]);
  });
  /* grunt.task.run('notify_hooks');*/
};
