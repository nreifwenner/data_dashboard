# Boilerplate Package

This is a simple boilerplate for Python packages.

# Requirements

* List
* of
* requirements

# Installation

Pip install:

    pip install -e git+GITURL#egg=REPO_NAME

If you use a pip requirements.txt file, add this line:

    -e git+GITURL#egg=REPO_NAME

# Usage

(place a link to your autodocs here)

# Development

Make sure you've set up your virtual environment to use Python VERSION

    # Example
    virtualenv-3.3 ENV
    source ENV/bin/activate

Clone PACKAGE_NAME from the Stash repo and run "make"

    git clone GITURL
    cd REPO_NAME
    make

## Testing

Run this command to execute tests

    make test

For more precise testing, use nosetest

    nosetests tests.py:TestCase.test_something_specific

## Generating autodocs

Navigate to the **"docs"** folder and run this command

    make html

See [Sphinx documentation](http://pythonhosted.org/an_example_pypi_project/sphinx.html) for more information.